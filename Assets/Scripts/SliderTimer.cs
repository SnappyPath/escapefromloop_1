﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderTimer : MonoBehaviour
{
    public int timer = 60;
    public Slider slider;

    private void Start()
    {
        slider.maxValue = timer;    
        slider.value = timer;
    }
    private void Update()
    {
        slider.value -= Time.deltaTime;
    }
}